# use an python runtime as parent image
FROM python:3.8-slim

#set the working directory in the container
WORKDIR /app

#copy the current directory contents into the container at /app
COPY App2.py /app
COPY Finance_data.csv /app

#install any needed package specified in requirements
RUN pip install pandas
RUN pip install pandas sqlalchemy 

# Specify SQLite as the database backend
RUN apt-get update && apt-get install -y sqlite3 libsqlite3-dev

# Run your Python script when the container launches
CMD ["python", "/app/App2.py"]
