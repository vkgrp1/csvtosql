#Jay Swaminarayan

#Python and SQL Data Engineer Project
import pandas as pd

from sqlalchemy import create_engine

##print ("Hello World")

df = pd.read_csv(filepath_or_buffer= r"./Finance_data.csv", delimiter=',', header=0)
print(df.head(5))

#engine = create_engine('mssql+pyodbc://username:password@server/database?driver=ODBC+Driver+17+for+SQL+Server')

database_file = 'mydatabase.db'

# Create SQLAlchemy engine for SQLite database
engine = create_engine(f'sqlite:///{database_file}')

df.to_sql('Finance', con =engine, if_exists='replace', index=False)

df1 = pd.read_sql_query('SELECT * FROM Finance', con=engine)

# Display the DataFrame (optional)
print(df1.head())


